import { takeLatest } from "redux-saga/effects"
import actionTypes from "../actions/types"
import { loginSaga, registerSaga } from "./auth.watcher"
import {fetchingScreenSaga } from "./screen.watcher"

export default function* saga(){
    yield takeLatest(actionTypes.LOGIN_REQUEST, loginSaga)
    yield takeLatest(actionTypes.REGISTER_REQUEST, registerSaga)
    yield takeLatest(actionTypes.FETCH_SCREEN_REQUEST, fetchingScreenSaga)
}