import { call, put } from 'redux-saga/effects'
import * as authActions from "../actions/authActions"
import {authApi} from '../api'

export function* loginSaga({payload}){
    try {
        const {data} = yield call(authApi, 'url', payload)
        let response = {email:'elijah.onduso@iprocu.re', password: '28015437zeffah0716199678'}
        if(response){
            yield put(authActions.actionLoginSuccess({ user: response, statusCode: 200, message: 'Success' }))
        }else {
            yield put(authActions.actionLoginFailed({ user: response, statusCode: 404, message: 'Not Found' }))
        }
    } catch (error) {
        yield put(authActions.actionLoginError({ user: {}, statusCode: 500, message: 'Server Error' }))
    }
}

export function* registerSaga({ payload }) {
    try {
        const {data} = yield call(authApi, 'url', payload)
        let response = { email: 'elijah.onduso@iprocu.re', password: '28015437zeffah0716199678', success: true }
        if (response) {
            yield put(authActions.registerSuccessAction({ user: response, statusCode: 200, message: 'Success' }))
        } else {
            yield put(authActions.registerFailedAction({ user: response, statusCode: 404, message: 'Not Found' }))
        }
    } catch (error) {
        yield put(authActions.registerErrorAction({ user: {}, statusCode: 500, message: 'Server Error' }))
    }
}