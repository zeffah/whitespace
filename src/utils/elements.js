import React from 'react'
import {Input, Select, Dropdown} from 'semantic-ui-react'
import { createField, fieldPresets } from 'react-advanced-form'
import DatePicker from 'react-datepicker'
const inputField = (props) => {
    const { fieldProps, fieldState } = props
    const { touched, errors, invalid, valid } = fieldState

    return (
        <div>
            {/* Propagating "fieldProps" is crucial to register a field */}
            <Input fluid {...fieldProps} />
            {touched && errors && errors.map((error) => (
                <div className="text--red">{error}</div>
            ))}
        </div>
    )
}

const customSelect = React.forwardRef((props, ref)=>{
    const { fieldProps, fieldState, options, placeholder, onChange } = props
    return (
        <Dropdown selection onChange={onChange} placeholder={placeholder} options={options} fluid />
    )
})

// const CustomSelect = (props) => {
//     const { fieldProps, fieldState, options, placeholder } = props
//     console.log(props)
//     return (
//         <Select placeholder={placeholder} options={options} fluid {...fieldProps}/>
//     )
// }

const CustomDatePicker = ({selected, onChange, placeholderText}) => (
    <DatePicker
        selected={selected}
        onChange={onChange}
        showTimeSelect
        showTimeSelectOnly
        timeIntervals={60}
        dateFormat="h:mm aa"
        timeCaption="Time"
        placeholderText={placeholderText} />
)

export default createField('DatePicker')(CustomDatePicker)

export const FormInput = createField(fieldPresets.input)(inputField)
export const FormSelect = createField()(customSelect)

