export const isObjectEmpty = (obj) => {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false
    }
    return true
}

export const updateScreensArray = (selectedScreens, _newScreen, plan) => {
    // _newScreen = { ..._newScreen, plan_rates: plan }
    const found = objectInArray(_newScreen, selectedScreens)
    let result = []
    if (!found) {
        result = [...selectedScreens, { ..._newScreen, plan: plan}]
    }else{
        result = selectedScreens.filter(screen => screen._id !== _newScreen._id)
    }
    return result;
}

export const objectInArray = (object, array)=>{
    return array.some(obj => obj._id === object._id)
}

export const getPlanByArgs = (plans, args) => {
    for (let i = 0; i < plans.length; i++) {
        let _plan = plans[i]
        if (_plan.plan_name === args || _plan._id === args || _plan.value === args) {
            return _plan
        }
    }
    return null
}

export const splitStringAndCapitalize = _string => {
    try {
        let _splitStringArray = _string.split("_")
        _splitStringArray.map(_word => capitalizeFirstLetter(_word));
        return _splitStringArray.join(" ")
    }catch(e){
        return _string
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}