import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { toast } from 'react-toastify';
import MainRouter from "./components/router"
import AppStore from "./store"
import 'react-toastify/dist/ReactToastify.css';
import 'react-day-picker/lib/style.css'

toast.configure();

class App extends Component {
  render() {
    return (
      <Provider store={AppStore}>
        <MainRouter />
        {/*<ToastContainer />*/}
      </Provider>
    );
  }
}

export default App;
