import React from 'react'
import { Link } from "react-router-dom"
import { Grid } from 'semantic-ui-react'
import { RegisterForm } from "./register.form"

export const RegisterMobile = () => {
    return (
        <Grid.Column floated='right' width={9} only='mobile'>
            <Grid columns={2} relaxed='very' stackable>
                <Grid.Column>
                    <RegisterForm/>
                </Grid.Column>
                <Grid.Column textAlign="center">
                    <Link to="/">Already have an account?Sign In</Link>
                </Grid.Column>
            </Grid>
        </Grid.Column>
    )
}