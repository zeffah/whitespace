import React, {Component} from "react"
import {Link} from "react-router-dom"
import {connect} from 'react-redux'
import axios from 'axios'
import history from './history'
import {Button, Divider, Form, Grid, Icon, List, Search} from "semantic-ui-react"
import {Header as AppHeader} from "./partials/header"
import {actionFetchScreenRequest} from '../actions/screenActions'
import {ScreenListComponent} from './screenList'
import {updateScreensArray} from '../utils/utils'

class LandingPageComponent extends Component {
    state = {plan: '', category: '', modalOpen:false, selected: [], plans: [], rawPlans: [], categories: [] }

    onChange = (e, props) => {
        const { name, value } = props
        this.setState({
            [name]: value
        })
    }

    historyPush = () => {
        history.push({
            pathname: '/screens/selected',
            state: this.state.selected
        })
    }

    getPlanById = (id, planList) => {
        for(let i = 0; i < planList.length; i++){
            let plan = planList[i].plan;
            if(plan._id === id){
                return plan
            }
        }
        return undefined
    }
    
    handleCardClick = (e, incomingScreen)=>{
        const {selected, plan} = this.state
        let incomingScreenPlanList = incomingScreen.plan_rates
        let planObject = this.getPlanById(plan, incomingScreenPlanList)
        if(plan){
            this.setState({
                selected: updateScreensArray(selected, incomingScreen, planObject)
            })
        }
    }

    render() {
        const {selected, plans, categories } = this.state
        const { onChange } = this
        const {screens} = this.props
        return (
            <Grid className="container ui" stackable columns={1}>
                <AppHeader loggedIn={true} user={'zeffah@gmail.com'}/>
                <Grid.Column textAlign='center'>
                    <Grid columns={2}>
                        <Grid.Column textAlign='right' width={10}>
                            <Search fluid
                                loading={false}
                                onResultSelect={() => { }}
                                onSearchChange={() => { }} />
                        </Grid.Column>
                        <Grid.Column textAlign='right' width={6}>
                            <Button as={Link} to='screens/approved' content='Approved Screens' style={{ color: '#fff', backgroundColor: '#2BB355'}} floated='right' />
                        </Grid.Column>
                    </Grid>
                </Grid.Column>
                <Grid.Column>
                    <Grid columns={2} stackable>
                        <Grid.Column width={4} style={{}}>
                            <Form>
                                <Form.Select name="category" fluid options={categories} onChange={(e, {value}) => console.log(value)} placeholder="Select Category"/>
                                <Form.Select name="plan" fluid options={plans} placeholder="Select Plan" onChange={onChange}/>
                            </Form><br/><br/><br/>
                            <Divider horizontal>Selected Screens</Divider>
                            {selected.length ? <ListExampleFloated selected={selected} />: <div>No Screen selected</div>}
                            {selected.length ? <Button onClick={this.historyPush} fluid positive content='Next' />:<div></div>}
                        </Grid.Column>
                        <Grid.Column style={{ backgroundColor: '' }} width={12}>
                            <ScreenListComponent screens={screens} selected={selected} handleCardClick={this.handleCardClick} />
                        </Grid.Column>
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }

    componentDidMount(){
        this.props.fetchScreens()
        axios.all([this.getPlans(), this.getCategories()])
            .then(axios.spread((_plans, _categories) => {
                const plans = _plans.data.map(plan => {
                    return { key: plan._id, value: plan._id, text: plan.plan_name }
                })

                const categories = _categories.data.map(category => {
                    return { key: category._id, value: category._id, text: category.category_name }
                })

                this.setState({
                    plans,
                    categories, 
                    rawPlans: _plans.data
                })
            }))
            .catch(error=>console.log(error))
    }

    getPlans = () => {
        return axios.get('http://localhost:5000/api/plans')
    }

    getCategories = () => {
        return axios.get('http://localhost:5000/api/categories')
    }
}

const mapDispatchToProps = {
    fetchScreens: actionFetchScreenRequest
}

const mapStateToProps = ({ screens}) => {
    return {
        screens: screens.screens
    }
}

const ListExampleFloated = ({selected}) => (
    <List divided verticalAlign='middle'>
    {
            (selected || []).map((screen)=>(
            <List.Item key={screen._id}>
                <List.Content floated='right'>
                    <Icon onClick={() => console.log('icon click listener')} color='red' name='times circle' />
                </List.Content>
                <List.Content>
                    <List.Header>{screen.screen_name}</List.Header>
                    <List.Description>{screen.category.category_name}</List.Description>
                </List.Content>
            </List.Item>
        ))
    }
    </List>
)

export default connect(mapStateToProps, mapDispatchToProps)(LandingPageComponent)