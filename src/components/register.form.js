import React from 'react'
import { Form } from 'semantic-ui-react'
import { ButtonRegister } from "./auth.button";

export const RegisterForm = ({ registerClick, onChange}) =>(
    <Form>
        <Form.Input onChange={onChange} name="mobile" icon='phone' iconPosition='left' type="phone" placeholder='Mobile Phone' />
        <Form.Input onChange={onChange} name="email" icon='mail' iconPosition='left' type="email" placeholder='Email Address' />
        <Form.Input onChange={onChange} name="password" icon='lock' iconPosition='left' placeholder='Password' type='password' />
        <Form.Input onChange={onChange} name="cpassword" icon='lock' iconPosition='left' placeholder='Confirm Password' type='password' />
        <ButtonRegister registerClick={registerClick} />
    </Form>
)