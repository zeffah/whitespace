import React from 'react'
import { Link } from "react-router-dom"
import { Grid } from 'semantic-ui-react'
import { RegisterForm } from "./register.form"

export const RegisterTablet = () => {
    return (
        <Grid.Column floated = 'right' width = { 9} only = 'tablet' >
            <Grid columns={1} relaxed='very' stackable>
                <Grid.Column width={16}>
                    {/* <Form>
                        <Form.Input icon='phone' iconPosition='left' type="phone" placeholder='Mobile Phone' />
                        <Form.Input icon='mail' iconPosition='left' type="email" placeholder='Email Address' />
                        <Form.Input icon='lock' iconPosition='left' placeholder='Password' type='password' />
                        <Form.Input icon='lock' iconPosition='left' placeholder='Confirm Password' type='password' />
                        <ButtonRegister />
                    </Form> */}
                    <RegisterForm/>
                </Grid.Column>
                <Grid.Column textAlign="center">
                    <Link to="/">Already have an account?Sign In</Link>
                </Grid.Column>
            </Grid>
        </Grid.Column>
    )
}