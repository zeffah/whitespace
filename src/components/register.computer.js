import React from 'react'
import { Link } from "react-router-dom";
import { Grid, Button, Divider } from 'semantic-ui-react'
import { RegisterForm } from "./register.form"

export const RegisterComputer = ({registerClick, goToLogin, onChange}) => (
    <Grid.Column floated='right' width={9} only='computer'>
        <Grid columns={2} relaxed='very' stackable>
            <Grid.Column>
                <RegisterForm registerClick={registerClick} onChange={onChange}/>
            </Grid.Column>
            <Grid.Column verticalAlign='middle' columns={1}>
                <Button as={Link} to='/login' fluid positive size="large" content="Login" icon='edit' />
            </Grid.Column>
        </Grid>
        <Divider vertical>Or</Divider>
    </Grid.Column>
)