import React from "react"
import { Grid, Table, Image, Header, Button, Label, Icon, Item } from "semantic-ui-react"
import NumberFormat from 'react-number-format'
import { Header as AppHeader } from "./partials/header"

export default class ScreenApproved extends React.Component {

    state = {
        upload: []
    }

    onUpload = (file) => {
        this.setState({
            upload: [...this.state.upload, file]
        })
    }

    render() {
        console.log(this.state.upload)
        return (
            <Grid className='container' columns='one'>
                <AppHeader />
                <Grid.Column width='16'>
                    <Table basic='very' celled collapsing>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Screen</Table.HeaderCell>
                                <Table.HeaderCell>Plan</Table.HeaderCell>
                                <Table.HeaderCell>Category</Table.HeaderCell>
                                <Table.HeaderCell>Approved</Table.HeaderCell>
                                <Table.HeaderCell>Comment/Recommendation </Table.HeaderCell>
                                <Table.HeaderCell></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            <Table.Row negative>
                                <Table.Cell>
                                    <Grid>
                                        <Grid.Column width='2'>
                                                <Image src='https://via.placeholder.com/100' rounded size='mini' />
                                        </Grid.Column>
                                        <Grid.Column width='14'>
                                            <Header as='h4'>
                                                <Header.Content>
                                                    Museum Hill Round about 3
                                                    <Header.Subheader>A high vehicle and pedestrian traffic area in Kileleshwa and lavington</Header.Subheader>
                                                </Header.Content>
                                            </Header>
                                        </Grid.Column>
                                    </Grid>
                                    {/* <Header as='h4' image>
                                        <Image src='https://via.placeholder.com/100' rounded size='mini' />
                                        <Header.Content>
                                            Museum Hill Round about 3
                                    <Header.Subheader>A high vehicle and pedestrian traffic area in Kileleshwa and lavington</Header.Subheader>
                                        </Header.Content>
                                    </Header> */}
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell negative singleLine>
                                    <Icon name='close' color='red' />
                                    Rejected
                                </Table.Cell>
                                <Table.Cell width='3'>Wrong content format. This screen requires content .jpg/png and size 80*120</Table.Cell>
                                <Table.Cell>
                                    <ButtonUpload onUpload={this.onUpload} content='Change Content'/>
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Grid>
                                        <Grid.Column width='2'>
                                            <Image src='https://via.placeholder.com/100' rounded size='mini' />
                                        </Grid.Column>
                                        <Grid.Column width='14'>
                                            <Header as='h4'>
                                                <Header.Content>
                                                    Museum Hill Round about 3
                                                    <Header.Subheader>A high vehicle and pedestrian traffic area in Kileleshwa and lavington</Header.Subheader>
                                                </Header.Content>
                                            </Header>
                                        </Grid.Column>
                                    </Grid>
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell positive singleLine>
                                    <Icon name='checkmark' color='green' />
                                    Approved
                                </Table.Cell>
                                <Table.Cell positive>Good</Table.Cell>
                                <Table.Cell>
                                    <ButtonUpload onUpload={this.onUpload} content='Change Content' />
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Grid>
                                        <Grid.Column width='2'>
                                            <Image src='https://via.placeholder.com/100' rounded size='mini' />
                                        </Grid.Column>
                                        <Grid.Column width='14'>
                                            <Header as='h4'>
                                                <Header.Content>
                                                    Museum Hill Round about 3
                                                    <Header.Subheader>A high vehicle and pedestrian traffic area in Kileleshwa and lavington</Header.Subheader>
                                                </Header.Content>
                                            </Header>
                                        </Grid.Column>
                                    </Grid>
                                </Table.Cell>
                                <Table.Cell singleLine>Long Term</Table.Cell>
                                <Table.Cell singleLine>Indoor Display</Table.Cell>
                                <Table.Cell positive singleLine>
                                    <Icon name='checkmark' color='green' />
                                    Approved
                                </Table.Cell>
                                <Table.Cell positive>Good</Table.Cell>
                                <Table.Cell>
                                    <ButtonUpload onUpload={this.onUpload} content='Change Content' />
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row negative>
                                <Table.Cell>
                                    <Grid>
                                        <Grid.Column width='2'>
                                            <Image src='https://via.placeholder.com/100' rounded size='mini' />
                                        </Grid.Column>
                                        <Grid.Column width='14'>
                                            <Header as='h4'>
                                                <Header.Content>
                                                    Museum Hill Round about 3
                                                    <Header.Subheader>A high vehicle and pedestrian traffic area in Kileleshwa and lavington</Header.Subheader>
                                                </Header.Content>
                                            </Header>
                                        </Grid.Column>
                                    </Grid>
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell negative singleLine>
                                    <Icon name='close' color='red' />
                                    Rejected
                                </Table.Cell>
                                <Table.Cell width='4'>Wrong content format. This screen requires content .jpg/png and size 80*120</Table.Cell>
                                <Table.Cell>
                                    {/* <UploadButton label='Upload Content' onUpload={{}} id='content_upload'/> */}
                                    <ButtonUpload onUpload={this.onUpload} content='Change Content'/>
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                        <Table.Footer fullWidth>
                            <Table.Row>
                                <Table.HeaderCell />
                                <Table.HeaderCell colSpan='5'>
                                    <Button floated='right' icon labelPosition='left' positive size='small'>
                                        <Icon name='checkmark' />Proceed
                                    </Button>
                                    <Button style={{ backgroundColor: 'transparent' }} size='small'>Total Cost:</Button>
                                    <Button style={{ backgroundColor: 'transparent' }}>
                                        <NumberFormat value={53088400} displayType={'text'} thousandSeparator={true} suffix={'/-'} />
                                    </Button>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </Grid.Column>
            </Grid>
        )
    }
}

const ButtonUpload = ({ onUpload, content }) => {
    let fileInput = null;
    return (
        <Label style={{ backgroundColor: 'transparent' }}
            as="label"
            htmlFor="upload">
            <Button
                icon="upload"
                label={{
                    basic: true,
                    content: content
                }}
                labelPosition="right" />
            <input
                hidden
                id="upload"
                type="file"
                onChange={(e) => onUpload(e.target.files[0])}
                ref={input => {
                    fileInput = input;
                }}
            />
        </Label>
    )
}
function UploadButton({ label, onUpload, id }) {
    let fileInput = null;
    // If no id was specified, generate a random one
    const uid = id || Math.random().toString(36).substring(7);

    return (
        <span>
            <label htmlFor={uid} className="ui icon button">
                <i className="upload icon"></i>
                {label}
            </label>
            <input type="file" id={uid}
                style={{ display: "none" }}
                onChange={() => {
                    onUpload(fileInput.files[0]);
                }}
                ref={input => {
                    fileInput = input;
                }}
            />
        </span>
    );
} 