import React from "react"
import axios from 'axios'
import {Redirect} from "react-router-dom";
import {Button, Grid, Header, Icon, Label, Table} from "semantic-ui-react"
import {DateUtils} from 'react-day-picker'
import { toast } from 'react-toastify';
import {Header as AppHeader} from "./partials/header"
import {ProgressLoader} from "./partials/loader";
import 'react-day-picker/lib/style.css';

export default class ScreenSelection extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            files: [],
            selectedDays: [],
            selectedScreens: props.location.state,
            loading: false,
            success: false
        };
    }


    objectToFormData = (obj, form, namespace) => {
        let fd = form || new FormData();
        let formKey;
        for(let key in obj) {
            if(obj.hasOwnProperty(key)) {
                if(namespace) {
                    formKey = [namespace, key,].join('');
                } else {
                    formKey = key;
                }
                // formKey = key;
                if(typeof obj[key] === 'object' && !(obj[key] instanceof File)) {
                    this.objectToFormData(obj[key], fd, formKey);
                } else {
                    fd.append(formKey, obj[key]);
                }

            }
        }
        return fd;
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({loading: true});
        // const {files, selectedScreens} = this.state;
        // let screens_ = selectedScreens.map(screen => {
        //     return {screen: screen._id, content: screen.file.name, plan: screen.plan._id, rate: screen.plan.rate}
        // })
        // const data = {
        //     files,
        //     selectedScreens: screens_
        // };
        //
        // const formData = this.formatFormData(data)
        // axios.request({
        //     method: 'POST',
        //     url: 'http://localhost:5000/api/bookings',
        //     data: formData
        // })
        // .catch(error => console.log(error))
        // .then(result => {
        //     console.log(result)
        //     this.showToast()
        // })
    };

    formatFormData = (data) => {

        const {files, selectedScreens} = data
        let formData = this.jsonToFormData(selectedScreens)
        for (let i = 0; i < files.length; i++){
            console.log(files[i])
            formData.append("files", files[i]);
        }

        return formData
    };

    buildFormData = (formData, data, parentKey) => {
        if (data && typeof data === 'object' && !(data instanceof Date) && !(data instanceof File)) {
            Object.keys(data).forEach(key => {
                this.buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
            });
        } else {
            const value = data == null ? '' : data;

            formData.append(parentKey, value);
        }
    }

    jsonToFormData = data => {
        const formData = new FormData();
        this.buildFormData(formData, data);
        return formData;
    }

    handleDayClick = this.handleDayClick.bind(this);

    handleRemoveScreen = screen => {
        let selectedScreens = this.state.selectedScreens

        selectedScreens = selectedScreens.filter(_item => {
            return _item._id !== screen._id
        })

        this.setState({
            selectedScreens
        })
    }

    onUpload = (file, _screen) => {
        let selectedScreens = this.state.selectedScreens
        selectedScreens = selectedScreens.map(screen => {
            if (screen._id === _screen._id) {
                screen = {...screen, file}
            }
            return screen
        })
        this.setState({
            files: [...this.state.files, file],
            selectedScreens
        })
    }

    handleDayClick(day, {selected}) {
        const {selectedDays} = this.state;
        if (selected) {
            const selectedIndex = selectedDays.findIndex(selectedDay =>
                DateUtils.isSameDay(selectedDay, day)
            );
            selectedDays.splice(selectedIndex, 1);
        } else {
            selectedDays.push(day);
        }
        this.setState({selectedDays});
    }

    showToast = () => toast('Sent successfully', {
        onClose: () => this.setState({
            success: true,
            loading: false
        })
    });

    render() {
        const {selectedScreens, files, success, loading} = this.state
        const {handleRemoveScreen} = this
        console.log(files)

        if (success === true) {
            return <Redirect to='/'/>
        }
        return (
            <Grid className='container' columns='one'>
                <AppHeader/>
                <ProgressLoader overlay={'container'} loading={this.state.loading}/>
                <Grid.Column width='16' className='container'>
                    <Table basic='very' celled collapsing>
                        <TableHeader/>
                        <Table.Body>
                            {
                                selectedScreens.map(screen => {
                                    return (
                                        <Table.Row key={screen._id}>
                                            <Table.Cell>
                                                <Header as='h4' image>
                                                    <Header.Content>
                                                        {screen.screen_name}
                                                        <Header.Subheader>{screen.description}</Header.Subheader>
                                                    </Header.Content>
                                                </Header>
                                            </Table.Cell>
                                            <Table.Cell>{screen.plan.plan_name}</Table.Cell>
                                            <Table.Cell>{screen.category.category_name}</Table.Cell>
                                            <Table.Cell textAlign='center'><Icon
                                                color={screen.file === undefined ? 'red' : 'green'}
                                                name={screen.file === undefined ? 'times circle' : 'check circle'}/></Table.Cell>
                                            <Table.Cell
                                                negative={screen.file === undefined}>{screen.file ? screen.file.name : 'No Content Uploaded'}</Table.Cell>
                                            <Table.Cell>
                                                <ButtonUpload onUpload={this.onUpload} currentScreen={screen}/>
                                                <Button color='red' content='Remove'
                                                        onClick={() => handleRemoveScreen(screen)}/>
                                            </Table.Cell>
                                        </Table.Row>
                                    )
                                })
                            }
                        </Table.Body>
                        <Table.Footer fullWidth>
                            <Table.Row>
                                <Table.HeaderCell/><Table.HeaderCell/><Table.HeaderCell/><Table.HeaderCell/><Table.HeaderCell/>
                                <Table.HeaderCell>
                                    <Button floated='right' icon labelPosition='left' primary size='small'
                                            onClick={this.handleSubmit}>
                                        <Icon name='check'/> Submit Content for Approval
                                    </Button>
                                    {/* <NumberFormat value={53088400} displayType={'text'} thousandSeparator={true} suffix={'/-'} /> */}
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </Grid.Column>
                {/* <Grid.Column width={3}> */}
                {/* <StripeProvider apiKey="pk_test_TYooMQauvdEDq54NiTphI7jx">
                        <div className="example">
                            <Elements>
                                <CheckoutForm />
                            </Elements>
                        </div>
                    </StripeProvider> */}
                {/* </Grid.Column> */}
            </Grid>
        )
    }
}

const TableHeader = () => (
    <Table.Header>
        <Table.Row>
            <Table.HeaderCell>Screen</Table.HeaderCell>
            <Table.HeaderCell>Plan</Table.HeaderCell>
            <Table.HeaderCell>Category</Table.HeaderCell>
            <Table.HeaderCell>Uploaded</Table.HeaderCell>
            <Table.HeaderCell>File Name</Table.HeaderCell>
            <Table.HeaderCell>Action</Table.HeaderCell>
        </Table.Row>
    </Table.Header>
)

const ButtonUpload = ({onUpload, currentScreen}) => {
    let fileInput = null;
    return (
        <Label style={{backgroundColor: 'transparent'}}
               as="label"
               htmlFor={currentScreen._id}>
            <Button
                icon="upload"
                label={{
                    basic: true,
                    content: 'Select file'
                }}
                labelPosition="right"/>
            <input
                hidden
                id={currentScreen._id}
                type="file"
                onChange={(e) => onUpload(e.target.files[0], currentScreen)}
                ref={input => {
                    fileInput = input;
                }}
            />
        </Label>
    )
}

function UploadButton({label, onUpload, id}) {
    let fileInput = null;
    // If no id was specified, generate a random one
    const uid = id || Math.random().toString(36).substring(7);

    return (
        <span>
            <label htmlFor={uid} className="ui icon button">
                <i className="upload icon"></i>
                {label}
            </label>
            <input type="file" id={uid}
                   style={{display: "none"}}
                   onChange={() => {
                       onUpload(fileInput.files[0]);
                   }}
                   ref={input => {
                       fileInput = input;
                   }}
            />
        </span>
    );
}