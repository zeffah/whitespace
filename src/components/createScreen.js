import React from 'react'
import axios from 'axios'
import { Grid, GridColumn, Button, Label, Dropdown } from 'semantic-ui-react'
import { Form, Field } from 'react-advanced-form'
import { Header } from './partials/header'
import DatePicker, {FormInput as Input, FormSelect as Select} from '../utils/elements'

export default class CreateScreen extends React.Component {
    state = {
        plan: '', 
        rate: '',
        plan_name: '',
        category: '',
        start_time: new Date(),
        end_time: new Date(),
        planGroups: [],
        prime:[],
        plans: [],
        categories: [],
        peakPeriod: []
    }

    handleStartDateChange = (date) => {
        console.log(date);
        
        this.setState({
            start_time: date
        })
    }

    handleEndDateChange = (date) => {
        this.setState({
            end_time: date
        })
    }

    handleChange = (e, {name, value}) => {
        console.log({[name]:value})
        this.setState({
            [name]:value
        })
    }

    onChange = (e) => {
        // console.log(e)
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    getPlanByName = (plans, planName) => {
        for(let i = 0; i < plans.length; i++){
            let _plan = plans[i]
            if (_plan.plan_name === planName){
                return _plan
            }
        }
        return undefined
    }

    onSubmit = ({serialized, fields, form}) => {
        const {plans, category} = this.state
        let plan_rates = []
        let object = serialized.plan_rates
        for (const key in object) {
            if (object.hasOwnProperty(key)) {
                const rate = object[key];
                let plan_en_rate = {
                    plan: { ...this.getPlanByName(plans, key), rate},
                    rate
                }
                plan_rates = [...plan_rates, plan_en_rate]
            }
        }
        let screeInfo = {
            plan_rates,
            screen_name: serialized.screen_name,
            screen_size: serialized.screen_size,
            description: serialized.description,
            category: category,
            location: serialized.location,
        }
        console.log('screensInfo: ', screeInfo)
        let promise = axios.post('http://localhost:5000/api/screens', screeInfo)
        promise.then(response => console.log(response)).catch(error => console.log(error))
        return promise;
    }

    onPrimePeriodSubmit = ({ serialized, fields, form }) => {
        serialized = { ...serialized, end_time: this.state.end_time, start_time: this.state.start_time}
        let promise = axios.post('http://localhost:5000/api/peakPeriod', serialized)
        promise.then(response => console.log(response)).catch(error => console.log(error))
        return promise
    }

    onPlanSubmit = ({ serialized, fields, form }) => {
        serialized = {...serialized, plan_group: this.state.plan_group}
        console.log(serialized)
        let promise = axios.post('http://localhost:5000/api/plans', serialized)
        promise.then(response => console.log(response)).catch(error => console.log(error))
        return promise
    }

    onPlanGroupSubmit = ({ serialized, fields, form }) => {
        let promise = axios.post('http://localhost:5000/api/planGroups', serialized)
        promise.then(response => console.log(response)).catch(error => console.log(error))
        return promise
    }

    onCategorySubmit = ({ serialized, fields, form }) => {
        let promise = axios.post('http://localhost:5000/api/categories', serialized)
        promise.then(response => console.log(response)).catch(error => console.log(error))
        return promise
    }

    render(){
        const { onSubmit, handleChange, onChange, getPlanByName } = this
        const { prime, planGroups, categories, peakPeriod, plans } = this.state
        const ref = React.createRef()
        console.log(plans)
        return(
            <Grid className='container'>
                <Header/>
                <Grid.Column width='10'>
                    <Form action={onSubmit}>
                        <Input name='screen_name' type='text' placeholder='Screen Name' /><br />
                        <Input name='screen_size' type='text' placeholder='Screen size eg 12ft by 8ft' /><br />
                        <Input name='description' type='text' placeholder='Screen Description' /><br />
                        <Input name='location' type='text' placeholder='Location' /><br />
                        {/* <Dropdown selection onChange={handleChange} name='plan' options={planGroups} placeholder="Select PlanGroup" /> */}
                        <Dropdown fluid selection onChange={handleChange} name='category' options={categories} placeholder="Select Category" /><br /><br/>
                        <label>Screen Plans</label><br/>
                        <Field.Group name="plan_rates">
                            <Input name='one_month_plan' type='number' placeholder='One-Month Plan' />
                            <Input name='one_week_plan' type='number' placeholder='One-Week Plan' />
                            <Input name='one_day_plan' type='number' placeholder='One-Day Plan' />
                            <Input name='one_hour_plan' type='number' placeholder='One-Hour Plan' />
                            {/* {
                                peakPeriod.map((time, index) => (
                                    <div key={index}>
                                        <label>{time.peak_name}</label>
                                        <Field.Group name={time.peak_name}>
                                            <div hidden>
                                                <Input name='peak' value={time._id} type='text' />
                                            </div>
                                            <Field.Group name="plan">
                                                <Input name='thirty_day_plan' type='number' placeholder='30 Day Plan' />
                                                <Input name='seven_day_plan' type='number' placeholder='7 Day Plan' />
                                                <Input name='one_day_plan' type='number' placeholder='1 Day Plan' />
                                                <Input name='one_hour_plan' type='number' placeholder='1 Hour Plan' />
                                            </Field.Group>
                                        </Field.Group> <br />
                                    </div>
                                ))
                            } */}
                            {/* {
                                plans.map((plan, i)=> (
                                    <div key={i}>
                                        <label>{plan.plan_name}</label> 
                                        <Field.Group name='rates'>
                                            <div hidden>
                                                <Input name='plan' value={plan._id} type='text' />
                                            </div>
                                            <Input name={`${plan.plan_name}-rate`} type='number' placeholder={`Cost for ${plan.plan_name}`} onChange={onChange} />
                                        </Field.Group>
                                    </div>
                                ))
                            } */}
                        </Field.Group>
                        <Button positive type='submit'>Submit</Button>
                    </Form>
                </Grid.Column>
                <Grid.Column width='6'>
                    <Form action={this.onPrimePeriodSubmit}>
                        <Input name="peak_name" type="text" placeholder='Peak Name' required />
                        {/* <Grid columns='2'>
                            <Grid.Column width={8}>
                                <DatePicker name='start_time'
                                    selected={this.state.start_time}
                                    onChange={this.handleStartDateChange}
                                    placeholderText='Start Time'
                                    fluid /><br />
                            </Grid.Column>
                            <Grid.Column width={8}>
                                <DatePicker name='start_time'
                                    selected={this.state.start_time}
                                    onChange={this.handleStartDateChange}
                                    placeholderText='Start Time'
                                    fluid /><br />
                            </Grid.Column>
                        </Grid> */}
                        <DatePicker name='start_time'
                            selected={this.state.start_time}
                            onChange={this.handleStartDateChange}
                            placeholderText='Start Time' 
                            fluid /><br/>

                        <DatePicker name='end_time'
                            placeholderText='End Time'
                            selected={this.state.end_time}
                            onChange={this.handleEndDateChange} 
                            style={{width: '100%'}} /><br/>
                        <Button fluid positive type='submit'>Submit</Button>
                    </Form><br/><br/>

                    <Form action={this.onPlanGroupSubmit}>
                        <Input name='plan_group_name' type='text' placeholder='Enter Plan Group name' />
                        <Button fluid positive type='submit'>Submit</Button>
                    </Form><br /><br />

                    <Form action={this.onPlanSubmit}>
                        <Dropdown fluid selection onChange={handleChange} name='plan_group' options={planGroups} placeholder="Select Plan Group" />
                        <Input name='plan_name' type='text' placeholder='Enter Plan name' />
                        <Button fluid positive type='submit'>Submit</Button>
                    </Form><br /><br />

                    <Form action={this.onCategorySubmit}>
                        <Input name='category_name' type='text' placeholder='Enter Category name' />
                        <Button fluid positive type='submit'>Submit</Button>
                    </Form>
                </Grid.Column>
            </Grid>
        )
    }

    componentDidMount(){
        axios.all([this.getPlanGroups(), this.getPlans(), this.getCategories(), this.getPeakPeriod()])
            .then(axios.spread((_plansGroups, _plans, _categories, peakPeriod) => {
                const planGroups = _plansGroups.data.map(planGroup => {
                    return { key: planGroup._id, value: planGroup._id, text: planGroup.plan_group_name }
                })

                const categories = _categories.data.map(category => {
                    return { key: category._id, value: category._id, text: category.category_name }
                })

                console.log(peakPeriod)

                this.setState({
                    planGroups,
                    categories,
                    plans: _plans.data,
                    peakPeriod: peakPeriod.data
                })
            }))
            .catch(error => console.log(error))
        // axios.get('http://localhost:5000/api/primePeriod')
        // .then(response => {
        //     let prime = response.data
        //     this.setState({
        //         prime
        //     })
        // })
        // .catch(error=>{
        //     return error
        // })
    }

    getPeakPeriod = () => {
        return axios.get('http://localhost:5000/api/peakPeriod')
    }

    getPlanGroups = () => {
        return axios.get('http://localhost:5000/api/planGroups')
    }

    getPlans = () => (
        axios.get('http://localhost:5000/api/plans')
    )

    // getPrimePeriod = () => {
    //     return axios.get('http://localhost:5000/api/peak_period')
    // }

    getCategories = () => {
        return axios.get('http://localhost:5000/api/categories')
    }
}