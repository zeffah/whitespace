import React from "react"
import { Grid, Card, Image} from "semantic-ui-react"
import {objectInArray} from '../utils/utils'

export const ScreenListComponent = ({screens, selected, handleCardClick}) => (
    <Grid stackable columns="three">
        {
            screens.map((screen, index) => (
                <Grid.Column key={index}>
                    <Card id={index} link color={objectInArray(screen, selected) ? 'red' : 'grey'} onClick={(e) => handleCardClick(e, screen)}>
                        <Image src='https://via.placeholder.com/150' />
                        <Card.Content style={{ color: '#2BB355' }}><b>{screen.screen_name}</b></Card.Content>
                        <Card.Content>
                            <Card.Description>
                            </Card.Description>
                            <Card.Description><span style={{ color: '#000000' }}><b>Size(Feet):&nbsp;</b></span>{screen.screen_size}</Card.Description><br />
                            <Card.Description><span style={{ color: '#000000' }}><b>Description:&nbsp;</b></span>{screen.description} - {screen.category.category_name}</Card.Description>
                        </Card.Content>
                        {/* <Card.Content extra>
                                    <Button onClick={selectScreen} inverted secondary floated='right'>
                                        <Icon name='check' />
                                        SELECT
                                    </Button>
                                </Card.Content> */}
                    </Card>
                </Grid.Column>
            ))
        }
    </Grid>
)