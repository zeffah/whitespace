import React from 'react'
import { Route, Router } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'
import LoginComponent  from "./login.component"
import RegisterComponent from "./register.component"
import LandingPageComponent from "./landing.component"
import SelectedScreens from './screen-selection.component'
import ApprovedScreens from './screen-approved.component'
import ScreenDateSelection from "./screen-dates.component"
import CreateScreen from './createScreen'
import history from './history'

const Main = () => (
    <Router history={history}>
        <Grid>
            <Route exact path="/" component={LandingPageComponent} />
            <Route path="/login" component={LoginComponent} />
            <Route path="/register" component={RegisterComponent} />
            <Route path='/screens/selected' component={SelectedScreens} />
            <Route path='/screens/approved' component={ApprovedScreens} />
            <Route path='/screens/select-booking-period' component={ScreenDateSelection} />
            <Route path='/screens/createScreen' component={CreateScreen} />
        </Grid>
    </Router>
)

export default Main