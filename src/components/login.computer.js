import React from 'react'
import { Link } from "react-router-dom"
import { Grid, Form,Button, Divider } from 'semantic-ui-react'
import { ButtonLogin } from "./auth.button";

export const LoginComputer = ({ loginClick, onChange }) => {
    return (
        <Grid.Column floated='right' width={9} only='computer'>
            <Grid columns={2} relaxed='very' stackable textAlign='center'>
                <Grid.Column>
                    <Form>
                        <Form.Input name="email" onChange={(e) => onChange(e)} icon='user' iconPosition='left' type="email" placeholder='Email Address' />
                        <Form.Input name="password" onChange={(e) => onChange(e)} icon='lock' iconPosition='left' placeholder='Password' type='password' />
                        <ButtonLogin loginClick={loginClick} /><br /><br />
                        <p>Don't have an Account?&nbsp;<Link to='/register'>Register Here</Link></p>
                    </Form>
                </Grid.Column>
                <Grid.Column verticalAlign='middle' columns={1}>
                    <Button fluid content='Google' icon="google" color="google plus" /><br />
                    <Button fluid color='facebook' content="Facebook" icon='facebook' /><br />
                    <Button fluid color='linkedin' content="LinkedIn" icon='linkedin' />
                </Grid.Column>
            </Grid>
            <Divider vertical>Or</Divider>
        </Grid.Column>
    )
}