import React from 'react'
import { connect } from 'react-redux'
import { Grid, Checkbox } from 'semantic-ui-react'
import { Header } from "./partials/header"
import { Slogan } from "./slogan"
import { LoginComputer } from "./login.computer"
import { LoginTablet } from "./login.tablet"
import { LoginMobile } from "./login.mobile"
import { actionLoginRequest } from "../actions/authActions"
import { isObjectEmpty } from "../utils/utils"

class LoginComponent extends React.Component {
    state = { email: "", password: "" }
    
    onChange = (e) => {
        e.preventDefault()
        this.setState({[e.target.name] : e.target.value})
    }

    onClick = () => {
        this.props.login(this.state)
    }

    render(){
        const {isAuthenticating, response} = this.props
        return (
            <Grid className="ui container" columns={1} stackable>
                <Header />
                <Grid.Column>
                    <Grid stackable columns={2}>
                        <Slogan text={'The affordable hassle-free way to book display advertising space; from your seat!'}/>
                        <LoginComputer loginClick={this.onClick} onChange={this.onChange}/>
                        <LoginTablet loginClick={this.onClick} />
                        <LoginMobile loginClick={this.onClick} />
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }
}
const mapDispatchToProps = {
    login:actionLoginRequest
}

const mapStateToProps = ({login}) => ({
        response: login.response,
        isAuthenticating:login.isAuthenticating
    }
)

export default connect(mapStateToProps, mapDispatchToProps)(LoginComponent)
