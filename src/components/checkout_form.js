import React, { Component } from 'react';
import { Elements, CardElement, CardNumberElement, CardCVCElement, CardExpiryElement, PostalCodeElement, injectStripe } from 'react-stripe-elements';
import {Button, Label} from 'semantic-ui-react'
import './checkout_form.css'

class CheckoutForm extends Component {
    constructor(props) {
        super(props)
        this.submit = this.submit.bind(this);
    }

    async submit(ev) {
        let { token } = await this.props.stripe.createToken({ name: "Name" });
        let response = await fetch("/charge", {
            method: "POST",
            headers: { "Content-Type": "text/plain" },
            body: token.id
        });

        if (response.ok) console.log("Purchase Complete!")
    }

    handleBlur = () => {
        console.log('[blur]');
    };
    handleChange = (change) => {
        console.log('[change]', change);
    };
    handleClick = () => {
        console.log('[click]');
    };
    handleFocus = () => {
        console.log('[focus]');
    };
    handleReady = () => {
        console.log('[ready]');
    };

    createOptions = (fontSize, padding) => {
        return {
            style: {
                base: {
                    fontSize,
                    color: '#424770',
                    letterSpacing: '0.025em',
                    fontFamily: 'Source Code Pro, monospace',
                    '::placeholder': {
                        color: '#aab7c4',
                    },
                    padding,
                },
                invalid: {
                    color: '#9e2146',
                },
            },
        };
    };

    render() {
        const {createOptions, handleBlur, handleChange, handleReady, handleFocus, handleClick} = this
        return (
            <Elements>
            <div className="checkout">
                <p>Would you like to complete the purchase?</p>
                {/* <CardElement /> */}
                    <label>
                        Card number
                        <CardNumberElement
                            onBlur={handleBlur}
                            onChange={handleChange}
                            onFocus={handleFocus}
                            onReady={handleReady}
                            {...createOptions(this.props.fontSize)}
                        />
                    </label>
                    <label>
                        Expiration date
                        <CardExpiryElement
                            onBlur={handleBlur}
                            onChange={handleChange}
                            onFocus={handleFocus}
                            onReady={handleReady}
                            {...createOptions(this.props.fontSize)}
                        />
                    </label>
                    <label>
                        CVC
                        <CardCVCElement
                            onBlur={handleBlur}
                            onChange={handleChange}
                            onFocus={handleFocus}
                            onReady={handleReady}
                            {...createOptions(this.props.fontSize)} />
                    </label>
                    <label>
                        Postal code
                        <PostalCodeElement
                            onBlur={handleBlur}
                            onChange={handleChange}
                            onFocus={handleFocus}
                            onReady={handleReady}
                            {...createOptions(this.props.fontSize)} />
                    </label>
                <Button primary fluid onClick={this.submit}>Send</Button>
            </div>
            </Elements>
        );
    }
}

export default injectStripe(CheckoutForm);