import React from "react"
import { Grid, Table, Image, Header, Button, Label, Icon, Item, Menu, Modal, Checkbox, Input, List } from "semantic-ui-react"
import NumberFormat from 'react-number-format'
import { Header as AppHeader } from "./partials/header"
import DayPicker, { DateUtils } from 'react-day-picker'

export default class ScreenDate extends React.Component {

    state = {
        open: false,
        upload: [],
        selectedDays: [],
        activeItem: 'inbox'
    }

    onUpload = (file) => {
        this.setState({
            upload: [...this.state.upload, file]
        })
    }

    handleSelectPeriodClick = () => {
        this.setState({ open: true })
    }

    handleDayClick = this.handleDayClick.bind(this);

    handleDayClick(day, { selected }) {
        const { selectedDays } = this.state;
        if (selected) {
            const selectedIndex = selectedDays.findIndex(selectedDay =>
                DateUtils.isSameDay(selectedDay, day)
            );
            selectedDays.splice(selectedIndex, 1);
        } else {
            selectedDays.push(day);
        }
        this.setState({ selectedDays });
    }

    onModalClose = () => {
        this.setState({open:false})
    }

    render() {
        const { activeItem, selectedDays, open } = this.state
        console.log(selectedDays)
        return (
            <Grid className='container' columns='one'>
                <AppHeader />
                <Grid.Column width='12' className='container'>
                    <Table basic='very' celled collapsing>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Screen</Table.HeaderCell>
                                <Table.HeaderCell>Plan</Table.HeaderCell>
                                <Table.HeaderCell>Category</Table.HeaderCell>
                                <Table.HeaderCell>Selected Period</Table.HeaderCell>
                                <Table.HeaderCell></Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as='h4' image>
                                        <Image src='https://via.placeholder.com/150' rounded size='mini' />
                                        <Header.Content>
                                            Museum Hill Round about 3
                                <Header.Subheader>A high vehicle and pedestrian traffic area</Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell negative>{'No Period Set'}</Table.Cell>
                                <Table.Cell>
                                    <Button icon='calendar' circular secondary content='Set Period' onClick={this.handleSelectPeriodClick} />
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as='h4' image>
                                        <Image src='https://via.placeholder.com/150' rounded size='mini' />
                                        <Header.Content>
                                            Museum Hill Round about 2
                                <Header.Subheader>A high vehicle and pedestrian traffic area</Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell negative>{'No Period Set'}</Table.Cell>
                                <Table.Cell>
                                    <Button icon='calendar' circular secondary content='Set Period' onClick={this.handleSelectPeriodClick}/>
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as='h4' image>
                                        <Image src='https://via.placeholder.com/150' rounded size='mini' />
                                        <Header.Content>
                                            Museum Hill Round about 1
                                <Header.Subheader>A high vehicle and pedestrian traffic area</Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell negative>{'No Period Set'}</Table.Cell>
                                <Table.Cell>
                                    <Button icon='calendar' circular secondary content='Set Period' onClick={this.handleSelectPeriodClick}/>
                                </Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>
                                    <Header as='h4' image>
                                        <Image src='https://via.placeholder.com/150' rounded size='mini' />
                                        <Header.Content>
                                            Museum Hill Round about 1
                                <Header.Subheader>A high vehicle and pedestrian traffic area</Header.Subheader>
                                        </Header.Content>
                                    </Header>
                                </Table.Cell>
                                <Table.Cell>Long Term</Table.Cell>
                                <Table.Cell>Indoor Display</Table.Cell>
                                <Table.Cell negative>{'No Period Set'}</Table.Cell>
                                <Table.Cell>
                                    <Button icon='calendar' circular secondary content='Set Period' onClick={this.handleSelectPeriodClick}/>
                                </Table.Cell>
                            </Table.Row>
                        </Table.Body>
                        <Table.Footer fullWidth>
                            <Table.Row>
                                <Table.HeaderCell />
                                <Table.HeaderCell colSpan='4'>
                                    <Button floated='right' icon labelPosition='left' positive size='small'>
                                        <Icon name='check' />Checkout
                                    </Button>
                                    <Button style={{ backgroundColor: 'transparent' }} size='small'>Total:</Button>
                                    <Button style={{ backgroundColor: 'transparent' }} floated='right'>
                                        <NumberFormat value={53088400} displayType={'text'} thousandSeparator={true} suffix={'/-'} />
                                    </Button>
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Footer>
                    </Table>
                </Grid.Column>
                <Grid.Column textAlign='center' width={4}>
                    Calendar for long term screens
                    <DayPicker
                        selectedDays={this.state.selectedDays}
                        onDayClick={this.handleDayClick} />
                    <Button content='Save' fluid positive floated='right'/>
                </Grid.Column>
                <ModalPeriodSelect open={open} onClose={this.onModalClose} selectedDays={this.state.selectedDays} onDayClick={this.handleDayClick}/>
            </Grid>
        )
    }
}

const ModalPeriodSelect = ({ open, onClose, selectedDays, onDayClick }) => {
    return (
        <Grid>
            <Modal size={'large'} open={open} onClose={onClose}>
                <Modal.Header>Select Period</Modal.Header>
                <Modal.Content>
                    <Grid columns='2'>
                        <Grid.Column textAlign='center' width={6}>
                            <DayPicker
                                selectedDays={selectedDays}
                                onDayClick={onDayClick}
                            />
                        </Grid.Column>
                        <Grid.Column width={10}>
                            <Table compact celled definition>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell />
                                        <Table.HeaderCell>Peak</Table.HeaderCell>
                                        <Table.HeaderCell>From</Table.HeaderCell>
                                        <Table.HeaderCell>To</Table.HeaderCell>
                                        <Table.HeaderCell>Rate/Hr</Table.HeaderCell>
                                        <Table.HeaderCell>Hrs</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>
                                    <Table.Row>
                                        <Table.Cell collapsing>
                                            <Checkbox slider />
                                        </Table.Cell>
                                        <Table.Cell>Morning</Table.Cell>
                                        <Table.Cell>6am</Table.Cell>
                                        <Table.Cell>10am</Table.Cell>
                                        <Table.Cell>100</Table.Cell>
                                        <Table.Cell>
                                            <List>
                                                <List.Item><Checkbox />6AM - 7AM</List.Item>
                                                <List.Item><Checkbox />7AM - 8AM</List.Item>
                                                <List.Item><Checkbox />8AM - 9AM</List.Item>
                                                <List.Item><Checkbox />9AM - 10AM</List.Item>
                                            </List>
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell collapsing>
                                            <Checkbox slider />
                                        </Table.Cell>
                                        <Table.Cell>Mid Morning</Table.Cell>
                                        <Table.Cell>10am</Table.Cell>
                                        <Table.Cell>12pm</Table.Cell>
                                        <Table.Cell>100</Table.Cell>
                                        <Table.Cell>
                                            <List>
                                                <List.Item><Checkbox />6AM - 7AM</List.Item>
                                                <List.Item><Checkbox />7AM - 8AM</List.Item>
                                                <List.Item><Checkbox />8AM - 9AM</List.Item>
                                                <List.Item><Checkbox />9AM - 10AM</List.Item>
                                            </List>
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell collapsing>
                                            <Checkbox slider />
                                        </Table.Cell>
                                        <Table.Cell>Afternoon</Table.Cell>
                                        <Table.Cell>12pm</Table.Cell>
                                        <Table.Cell>3pm</Table.Cell>
                                        <Table.Cell>100</Table.Cell>
                                        <Table.Cell>
                                            <List>
                                                <List.Item><Checkbox />6AM - 7AM</List.Item>
                                                <List.Item><Checkbox />7AM - 8AM</List.Item>
                                                <List.Item><Checkbox />8AM - 9AM</List.Item>
                                                <List.Item><Checkbox />9AM - 10AM</List.Item>
                                            </List>
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell collapsing>
                                            <Checkbox slider />
                                        </Table.Cell>
                                        <Table.Cell>Evening</Table.Cell>
                                        <Table.Cell>3pm</Table.Cell>
                                        <Table.Cell>10pm</Table.Cell>
                                        <Table.Cell>100</Table.Cell>
                                        <Table.Cell>
                                            <List>
                                                <List.Item><Checkbox />6AM - 7AM</List.Item>
                                                <List.Item><Checkbox />7AM - 8AM</List.Item>
                                                <List.Item><Checkbox />8AM - 9AM</List.Item>
                                                <List.Item><Checkbox />9AM - 10AM</List.Item>
                                            </List>
                                        </Table.Cell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.Cell collapsing>
                                            <Checkbox slider />
                                        </Table.Cell>
                                        <Table.Cell>Late Night</Table.Cell>
                                        <Table.Cell>10pm</Table.Cell>
                                        <Table.Cell>6am</Table.Cell>
                                        <Table.Cell>100</Table.Cell>
                                        <Table.Cell>
                                            <List>
                                                <List.Item><Checkbox />6AM - 7AM</List.Item>
                                                <List.Item><Checkbox />7AM - 8AM</List.Item>
                                                <List.Item><Checkbox />8AM - 9AM</List.Item>
                                                <List.Item><Checkbox />9AM - 10AM</List.Item>
                                            </List>
                                        </Table.Cell>
                                    </Table.Row>
                                </Table.Body>

                                <Table.Footer fullWidth>
                                    <Table.Row>
                                        <Table.HeaderCell />
                                        <Table.HeaderCell colSpan='5'>
                                            <Button floated='right' icon labelPosition='left' secondary size='small'>
                                                <Icon name='check' /> Add Period
                                            </Button>
                                            {/* <Button size='small'>Approve</Button> */}
                                            {/* <Button disabled size='small'>
                                                Approve All
                                            </Button> */}
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>
                        </Grid.Column>
                    </Grid>
                </Modal.Content>
                <Modal.Actions>
                    <Button negative>Cancel</Button>
                    <Button positive icon='checkmark' labelPosition='right' content='Add' />
                </Modal.Actions>
            </Modal>
        </Grid>
    )
}


const ButtonUpload = ({ onUpload }) => {
    let fileInput = null;
    return (
        <Label style={{ backgroundColor: 'transparent' }}
            as="label"
            htmlFor="upload">
            <Button
                icon="upload"
                label={{
                    basic: true,
                    content: 'Select file'
                }}
                labelPosition="right" />
            <input
                hidden
                id="upload"
                type="file"
                onChange={(e) => onUpload(e.target.files[0])}
                ref={input => {
                    fileInput = input;
                }}
            />
        </Label>
    )
}
function UploadButton({ label, onUpload, id }) {
    let fileInput = null;
    // If no id was specified, generate a random one
    const uid = id || Math.random().toString(36).substring(7);

    return (
        <span>
            <label htmlFor={uid} className="ui icon button">
                <i className="upload icon"></i>
                {label}
            </label>
            <input type="file" id={uid}
                style={{ display: "none" }}
                onChange={() => {
                    onUpload(fileInput.files[0]);
                }}
                ref={input => {
                    fileInput = input;
                }}
            />
        </span>
    );
} 