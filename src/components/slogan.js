import React from 'react'
import { Grid } from 'semantic-ui-react'

export const Slogan = ({text}) => (
    <Grid.Column floated='left' width={7}>
        <p style={{ fontSize: "30px" }}>
            {text}
        </p>
    </Grid.Column>
)