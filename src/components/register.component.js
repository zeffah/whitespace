import React from 'react'
import { connect } from 'react-redux'
import { Link } from "react-router-dom"
import { Grid, Checkbox } from 'semantic-ui-react'
import { Header } from "./partials/header"
import { Slogan } from "./slogan"
import { RegisterComputer } from "./register.computer"
import { RegisterTablet } from "./register.tablet"
import { RegisterMobile } from "./register.mobile";
import { registerRequestAction } from "../actions/authActions"

class RegisterComponent extends React.Component {
    state = { email: "", password: "", mobile: "" }

    onChange = (e, these) => {
        e.preventDefault()
        this.setState({ [e.target.name]: e.target.value })
    }

    registerClick = () => {
        this.props.register(this.state)
    }

    goToLogin = () => {
        // this.props.register(this.state)
    }

    render(){
        return (
            <Grid className="ui container" columns={1} stackable>
                <Header />
                <Grid.Column>
                    <Grid stackable columns={2}>
                        <Slogan text={'The affordable hassle-free way to book display advertising space; from your seat!'}/>
                        <RegisterComputer registerClick={this.onClick} onChange={this.onChange}/>
                        <RegisterTablet />
                        <RegisterMobile />
                    </Grid>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapDispatchToProps = {
    register: registerRequestAction
}

const mapStateToProps = (state) => (
    console.log(state.register)
    // {
        // response: login.response,
        // isAuthenticating: login.isAuthenticating
    // }
)

export default connect(mapStateToProps, mapDispatchToProps)(RegisterComponent)
