import React from 'react'
import { Button } from 'semantic-ui-react'

export const ButtonLogin = ({loginClick, size}) => (
    <Button onClick={loginClick} size={size} fluid content='Login' floated="right" positive />
)

export const ButtonRegister = ({ registerClick }) => (
    <Button onClick={registerClick} fluid content='Register' floated="right" primary />
)