import React from 'react'
import { Grid, Form, Button } from 'semantic-ui-react'
// import GoogleLogin from 'react-google-login'
import { ButtonLogin } from "./auth.button"

export const LoginTablet = (props) => {
    // const responseGoogle = (response) => (
    //     console.log(response)
    // )
    return (
        <Grid.Column floated = 'right' width = { 9} only = 'tablet' >
            <Grid columns={1} relaxed='very' stackable>
                <Grid.Column width={16}>
                    <Form>
                        <Form.Input icon='user' iconPosition='left' type="email" placeholder='Email Address' />
                        <Form.Input icon='lock' iconPosition='left' placeholder='Password' type='password' />
                        <ButtonLogin loginClick={props.loginClick} />
                    </Form>
                </Grid.Column>
                <Grid.Column width={16}>
                    <Grid centered>
                        {/* <GoogleLogin
                            clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
                            render={renderProps => (
                                <Button circular onClick={renderProps.onClick} icon="google" color="google plus" />
                            )}
                            buttonText="Login"
                            onSuccess={responseGoogle}
                            onFailure={responseGoogle} /> */}
                        <Button circular icon="google" color="google plus" />
                        <Button circular color='facebook' icon='facebook' />
                        <Button circular color='linkedin' icon='linkedin' />
                    </Grid>
                </Grid.Column>
            </Grid>
        </Grid.Column>
    )
}