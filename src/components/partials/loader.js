import React from 'react'
import LoadingOverlay from 'react-loading-overlay';
import {BarLoader, BeatLoader} from "react-spinners";

export const ProgressLoader =({loading, overlay, children})=> {

    return (
        <LoadingOverlay overlay={overlay} active={loading} spinner={<BeatLoader/>} text='Sending Content...'>
            {children}
            {/*<p>Please wait while we submit your content to server.</p>*/}
        </LoadingOverlay>
    )
}