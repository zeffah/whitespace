import actionType from "./types"

export const actionLoginRequest = (data) => ({
        type:actionType.LOGIN_REQUEST,
        payload: data
    }
)

export const actionLoginSuccess = (response) => ({
        type: actionType.LOGIN_SUCCESS,
        payload: response
    }
)

export const actionLoginFailed = (response) => ({
        type: actionType.LOGIN_FAILED,
        message: response.message
    }
)

export const actionLoginError = (response) => ({
        type: actionType.LOGIN_ERROR,
        message: response.message
    }
)

export const registerRequestAction = (data) => ({
        type: actionType.REGISTER_REQUEST,
        payload: data
    }
)

export const registerSuccessAction = (response) => ({
        type: actionType.REGISTER_SUCCESS,
        payload: response
    }
)

export const registerFailedAction = (response) => ({
    type: actionType.REGISTER_FAILED,
    payload: response
})

export const registerErrorAction = (response) => ({
    type: actionType.REGISTER_ERROR,
    payload: response
})