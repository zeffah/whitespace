import actions from "../actions/types"

const initial = {
    response: {},
    isAuthenticating: false,
    payload: {}
}

export const userLogin = (state = initial, action)=> {
    switch (action.type) {
        case actions.LOGIN_REQUEST:
            return {...state, isAuthenticating: true, payload: action.payload }

        case actions.LOGIN_SUCCESS:
            return { ...state, isAuthenticating: false, response: action.payload }

        case actions.LOGIN_ERROR:
            return { ...state, isAuthenticating: false, response: action.payload }

        case actions.LOGIN_FAILED:
            return { ...state, isAuthenticating: false, response: action.payload }

        default:
            return state
    }
}

export const userSignUp = (state = initial, action) => {
    switch (action.type) {
        case actions.REGISTER_REQUEST:
            return { ...state, isAuthenticating: true, payload: action.payload }

        case actions.REGISTER_SUCCESS:
            return { ...state, isAuthenticating: false, response: action.payload }

        case actions.REGISTER_ERROR:
            return { ...state, isAuthenticating: false, response: action.payload }

        case actions.REGISTER_FAILED:
            return { ...state, isAuthenticating: false, response: action.payload }
        default:
            return state
    }
}