import { combineReducers } from "redux"
import { userLogin, userSignUp } from "./auth.reducer"
import { screensFetch } from "./screen.reducer"
export const rootReducer = combineReducers({
    login: userLogin,
    register: userSignUp,
    screens: screensFetch
})